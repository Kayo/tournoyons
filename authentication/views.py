from authentication.serializers import UserSerializer, CreateUserSerializer
from authentication.models import User
from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import CreateModelMixin, UpdateModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import action
from rest_framework.response import Response

#class UserViewset(ModelViewSet):
#
#    serializer_class = UserSerializer
#    permission_classes = [IsAuthenticated]
#
#    def get_queryset(self):
#        return User.objects.all()

class UserViewset(GenericViewSet, CreateModelMixin):

    serializer_class = UserSerializer

    def get_serializer_class(self):
        if self.action == 'create':
            return CreateUserSerializer
        return UserSerializer

    @action(detail=False, permission_classes = [IsAuthenticated])
    def current_user(self, request):
        current_user = User.objects.filter(id=self.request.user.id)[0]
        serializer = self.get_serializer(current_user)
        return Response(serializer.data)

    #@action(detail=False, methods=['put'], permission_classes = [IsAuthenticated])
    #def update_user(self, request):
    #    current_user = User.objects.filter(id=self.request.user.id)[0]
    #    if request.data['profile_picture']:
    #        current_user.profile_picture = request.data['profile_picture']
    #    serializer = self.get_serializer(current_user)
    #    return Response(serializer.data)