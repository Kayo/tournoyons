from authentication.models import User
from rest_framework.serializers import ModelSerializer, Serializer
from rest_framework import serializers
from django.contrib.auth.hashers import make_password

class UserSerializer(ModelSerializer):

    class Meta:
        model = User
        fields = ['username'] #'profile_picture']


class CreateUserSerializer(ModelSerializer):

    password = serializers.CharField(
        write_only=True,
        required=True,
        style={'input_type': 'password'}
    )

    password2 = serializers.CharField(
        write_only=True,
        required=True,
        style={'input_type': 'password'}
    )

    class Meta:
        model = User
        fields = ['username', 'password', 'password2', 'email']

    def validate(self, data):
         # Il faut tester que les deux sont pareils
        if (data['password'] != data['password2']):
            raise serializers.ValidationError('Password validation failed')

        # data['password'] = (data.get('password'))

        # Puis suppression du champ inutile
        data.pop('password2')

        # Ajout du champ image avec la valeur par défaut
        #data['profile_picture'] = 

        return data
