from django.urls import include, path
from rest_framework import routers
from authentication.views import UserViewset
from tournoyons.views import TournamentViewset, RoundViewset, PlayerViewset, MatchViewset
from rest_framework_nested import routers
from django.conf import settings
from django.conf.urls.static import static
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

router = routers.SimpleRouter()
router.register('tournament', TournamentViewset, basename='tournament')
router.register('user', UserViewset, basename='user')

player_router = routers.NestedSimpleRouter(router, r'tournament', lookup='tournament')
player_router.register(r'player', PlayerViewset, basename='player')

round_router = routers.NestedSimpleRouter(router, r'tournament', lookup='tournament')
round_router.register(r'round', RoundViewset, basename='round')

match_router = routers.NestedSimpleRouter(round_router, r'round', lookup='round')
match_router.register(r'match', MatchViewset, basename='match')

urlpatterns = [
    path('', include(router.urls)),
    path('', include(player_router.urls)),
    path('', include(round_router.urls)),
    path('', include(match_router.urls)),
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh')
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)