# Tournoyons !

Backend pour gérer des tournois

# Utiliser le dépot

```
python3 -m venv env
source env/bin/activate

pip install -r requirements.txt

python manage.py runserver
```

# Précaution

- Requêtes CORS activé pour localhost
