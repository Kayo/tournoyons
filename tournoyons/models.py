from django.db import models
from django.conf import settings
from django.core.validators import MaxValueValidator, MinValueValidator
from django.utils.translation import gettext_lazy as _


# Représente un tournois dans son intégralité
class Tournament(models.Model):
    
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    date_start = models.DateTimeField(blank=True, null=True)
    date_end = models.DateTimeField(blank=True, null=True)
    nb_max_player = models.IntegerField(validators=[MinValueValidator(4), MaxValueValidator(1024)])
    nb_player_by_round = models.IntegerField(validators=[MinValueValidator(2), MaxValueValidator(8)])
    image_url = models.ImageField(upload_to='tournaments', blank=True, null=True)

# Représente un joueur qui participe à un tournois
# Le joueur peut être lié à un utilisateur ou non (joueur "anonyme")
class Player(models.Model):
    
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE)
    pseudo = models.CharField(max_length=255, unique=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, blank=True, null=True)

# Représente un round d'un tournoi
# Peut être de 3 types différent
class Round(models.Model):

    class RoundType(models.TextChoices):
        GROUP = 'GR', _('Group')
        ELIMINATION = 'EL', _('Elimination')
        SWISS = 'SW', _('Swiss')

    round_type = models.CharField(max_length=2, choices=RoundType.choices)
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE)
    order = models.IntegerField(validators=[MinValueValidator(0)])

    class Meta:
        constraints = [models.UniqueConstraint(fields=['tournament', 'order'], name='unique_tournament_order')]

# Réprésente un match dans une étape
class Match(models.Model):

    class MatchState(models.TextChoices):
        COMPLETED = 'CO', _('Completed')
        PENDING = 'PE', _('Pending')
        PROCESSING = 'PR', _('Processing')

    round = models.ForeignKey(Round, on_delete=models.CASCADE)
    date_start = models.DateTimeField(blank=True, null=True)
    match_state = models.CharField(max_length=2, choices=MatchState.choices, 
                                   default=MatchState.PENDING)


#  Représente la participation d'un joueur à un match
class MatchRegistration(models.Model):

    match = models.ForeignKey(Match, on_delete=models.CASCADE)
    player = models.ForeignKey(Player, on_delete=models.CASCADE)


########################################
# Poule

# Représente une poule
class Group(models.Model):

    round = models.ForeignKey(Round, on_delete=models.CASCADE)
    group_name = models.CharField(max_length=255)

# Représente l'inscription à une poule
class GroupRegistration(models.Model):

    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    score = models.IntegerField(validators=[MinValueValidator(0)])


########################################
# Elimination

# Représente un match par élimination
class Elimination(models.Model):

    match = models.ForeignKey(Match, on_delete=models.CASCADE)
    round = models.ForeignKey(Round, on_delete=models.CASCADE)
    winner = models.ForeignKey(Player, on_delete=models.CASCADE)


# Représente tous les matchs "avant" auxquelles 
# Le match en cours est lié
class EliminationRegistration(models.Model):
 
    elimination = models.ForeignKey(Elimination, on_delete=models.CASCADE)
    match_parent = models.ForeignKey(Match, on_delete=models.CASCADE)

########################################
# Système suisse

# Représente l'inscription d'un joueur à un tournois Swiss
class Swiss(models.Model):

    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    round = models.ForeignKey(Round, on_delete=models.CASCADE)
    score = models.IntegerField(validators=[MinValueValidator(0)])
