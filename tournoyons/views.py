from django.contrib.auth import login
from django.shortcuts import get_object_or_404

from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from rest_framework.viewsets import ModelViewSet
from tournoyons.models import Tournament, Player, Round, Match, MatchRegistration
from tournoyons.serializers import TournamentSerializer, PlayerSerializer, RoundSerializer, MatchSerializer, MatchCreateSerializer, PlayerCreateSerializer, RoundCreateSerializer


class TournamentViewset(ModelViewSet):
 
    serializer_class = TournamentSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return Tournament.objects.all()


class RoundViewset(ModelViewSet):
 
    serializer_class = RoundSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return Round.objects.filter(tournament=self.kwargs['tournament_pk'])

    def get_serializer_class(self):
        if self.action == 'create':
            return RoundCreateSerializer
        return RoundSerializer

    def perform_create(self, serializer):
        tournament = get_object_or_404(Tournament, pk=self.kwargs['tournament_pk'])
        serializer.save(tournament=tournament)


class PlayerViewset(ModelViewSet):
 
    serializer_class = PlayerSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return Player.objects.filter(tournament=self.kwargs['tournament_pk'])

    def get_serializer_class(self):
        if self.action == 'create':
            return PlayerCreateSerializer
        return PlayerSerializer

    def perform_create(self, serializer):
        tournament = get_object_or_404(Tournament, pk=self.kwargs['tournament_pk'])
        serializer.save(tournament=tournament)


class MatchViewset(ModelViewSet):
 
    serializer_class = MatchSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return Match.objects.filter(tournament=self.kwargs['tournament_pk'],
                                    round=self.kwargs['round_pk'])

    def get_serializer_class(self):
        if self.action == 'create':
            return MatchCreateSerializer
        return MatchSerializer

    def perform_create(self, serializer):
        tournament = get_object_or_404(Tournament, pk=self.kwargs['tournament_pk'])
        round =  get_object_or_404(Round, pk=self.kwargs['round_pk'])
        serializer.save(tournament=tournament, round=round)

