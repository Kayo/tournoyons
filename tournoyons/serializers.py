from rest_framework.serializers import ModelSerializer

from tournoyons.models import Tournament, Player, Round, Match, MatchRegistration, Group, GroupRegistration


class TournamentSerializer(ModelSerializer):

    class Meta:
        model = Tournament 
        fields = '__all__'

class RoundSerializer(ModelSerializer):

    class Meta:
        model = Round 
        fields = '__all__'

class RoundCreateSerializer(ModelSerializer):

    class Meta:
        model = Round 
        exclude = ['tournament']

class PlayerSerializer(ModelSerializer):

    class Meta:
        model = Player 
        fields = '__all__'

class PlayerCreateSerializer(ModelSerializer):

    class Meta:
        model = Player 
        exclude = ['tournament']


class MatchSerializer(ModelSerializer):

    class Meta:
        model = Match 
        fields = '__all__'


class MatchCreateSerializer(ModelSerializer):

    class Meta:
        model = Match 
        exclude = ['tournament', 'round']


class GroupSerializer(ModelSerializer):

    class Meta:
        model = Group 
        fields = '__all__'


class GroupCreateSerializer(ModelSerializer):

    class Meta:
        model = Group 
        exclude = ['round']


class GroupRegistrationSerializer(ModelSerializer):

    class Meta:
        model = GroupRegistration 
        fields = '__all__'