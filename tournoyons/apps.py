from django.apps import AppConfig


class TournoyonsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'tournoyons'
