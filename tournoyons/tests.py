from rest_framework import status
from rest_framework.test import APITestCase
from tournoyons.models import Tournament

class TournamentTests(APITestCase):

#    def setUp(self):
        

    def test_create_tournament(self):
        """
        Ensure we can create a new tournament object.
        """
        url = '/tournament/'
        data = {
            'name': 'Nom de tournois',
            'description': 'Description de tournois',
            'nb_max_player': 5,
            'nb_player_by_round': 3
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Tournament.objects.count(), 1)
        self.assertEqual(Tournament.objects.get().name, 'Nom de tournois')

        
